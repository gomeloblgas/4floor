'use strict';

let regionBlock = document.querySelector(".region-info-block");
let regionNameBlock = document.querySelector(".region-name-block");
let descriptionBlock = document.querySelector(".region-description-block");
let selectedRegion = document.querySelector("a.js-svg-map.svg-map--active");
let regionQualityImprovementPlanBlock = document.querySelector(".nav-link.region-quality-improvement-plan");
let regionAchievementsBlock = document.querySelector(".region-achievements-block");
let allInfoBlock = document.querySelector(".all-region-info-block");

let mainControl = document.querySelector("#main-control");
let iframe = document.querySelector("#modal-iframe");
let modalTitle = document.querySelector(".modal-title");
let modalImg = document.querySelector(".modal-img");

let visibleBlock = document.querySelector('.properties .nav-link.active');

let gasCounterValue = document.querySelector('.gas-counter-value');
let gasCounterLabel = document.querySelector('.gas-counter-label');

// Variables for gas counter
let intervale = 0;
let gasInSecond = 0;
var currentGasValue = 0;
var isFirstLoop = true;

const getGasCounterValues = () => {
  fetch(`/4floor/server/apis/get-gas-counter.php`)
    .then(response => {
      return response.json();
    })
    .then(resp => {
      if (resp.code == 200) {
        parseGasCounterValues(resp.data);
      }
    });
}

const parseGasCounterValues = (data) => {
  let now = new Date();
  gasCounterLabel.innerText = `Реализовано газа за ${now.getFullYear()} год`;

  let dateStart = new Date(data['year'], data['current-month']); // менять месяц на текущий (нумерация 0..11)
  intervale = ((now.getTime() - dateStart.getTime()) - (now.getTime() - dateStart.getTime()) % 1000) / 1000;

  gasInSecond = ((data['gas-sales-plan'] - data['gas-sold']) / 30 / 24 / 3600).toFixed(2);
  currentGasValue = Math.round(intervale * gasInSecond) + parseInt(data['gas-sold']);

  gasCounter();
}

const openModalIframe = () => {
  $('.modal').modal();
}

const setDataForModalIframe = (title, link) => {
  modalTitle.innerText = title;
  iframe.src = link;
  iframe.classList.remove("unvisible");
  modalImg.classList.add("unvisible");
}

const setImageForModalIframe = (title, link) => {
  modalTitle.innerText = title;
  modalImg.src = link;
  modalImg.classList.remove("unvisible");
  iframe.classList.add("unvisible");
}

const numberFormat = (num) => {
  if (!isFinite(num)) {
      return num;
  }
  
  var parts = num.toString().split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

  return parts.join(', ');
};

const gasCounter = () => {
  currentGasValue = Math.round(parseFloat(currentGasValue) + parseFloat(gasInSecond));//.toFixed(2);
  gasCounterValue.innerText = numberFormat(currentGasValue);
  setTimeout(gasCounter, 980);
};

window.onload = () => {
  getGasCounterValues();

  mainControl.addEventListener("click", () => {
    setDataForModalIframe("Сайт предприятия", "https://gomeloblgaz.by")  
    $('.modal').modal();
  });
  //
  // let panoramaLink = document.querySelector("div#panoramaLink");
  // panoramaLink.addEventListener("click", () => {
  //   setDataForModalIframe("Схема газоснабжения", "http://192.168.0.251/cgi-bin/panorama/pipe_out.pl");
  //   $('.modal').modal();
  // });
  //
  let mapLink = document.querySelector("div#mapLink");
  mapLink.addEventListener("click", () => {
    setImageForModalIframe("Карта газоснабжения", "img/region-map.jpg");
    $('.modal').modal();
  });

  let gpoLink = document.querySelector("div#gpoLink");
  gpoLink.addEventListener("click", () => {
    setDataForModalIframe("ГПО \"Белтопгаз\"", "http://www.topgas.by");
    $('.modal').modal();
  });

  let minLink = document.querySelector("div#minLink");
  minLink.addEventListener("click", () => {
    setDataForModalIframe("Министерство энергетики", "https://minenergo.gov.by");
    $('.modal').modal();
  });

  if (urlParams.get('flag') != undefined) {
    let block = document.querySelector('#humanResources');
    block.classList.add('unvisible');
    document.querySelector('#specifications').classList.add('col-md-12');
  }
}