'use strict';

let regionBlock = document.querySelector(".region-info-block");
let regionNameBlock = document.querySelector(".region-name-block");
let descriptionBlock = document.querySelector(".region-description-block");
let selectedRegion = document.querySelector("a.js-svg-map.svg-map--active");
let regionQualityImprovementPlanBlock = document.querySelector(".nav-link.region-quality-improvement-plan");
let regionAchievementsBlock = document.querySelector(".region-achievements-block");
let allInfoBlock = document.querySelector(".all-region-info-block");

let mainControl = document.querySelector("#main-control");
let iframe = document.querySelector("#modal-iframe");
let modalTitle = document.querySelector(".modal-title");

let visibleBlock = document.querySelector('.properties .nav-link.active');

function setActionOnMap() {
  let regions = document.querySelectorAll("a.js-svg-map");
  regions.forEach(region => {
    region.addEventListener("click", (e) => {
      let element;

      if (e.target.localName == "path") {
        console.log(e.target.parentElement.dataset["tab"]);
        element = e.target.parentElement;

      } else if (e.target.localName == "a") {
        console.log(e.target.dataset["tab"]);
        element = e.target;
      }

      updateSelectedRegion(element);
    })
  });
}

const updateSelectedRegion = (element) => {
  selectedRegion.classList.remove("svg-map--active");
  element.classList.add("svg-map--active");
  selectedRegion = element;

  let id = element.dataset["tab"];
  getRegionInfo(id);
  getRegionAchievements(id); 
}

function addActionToPropertiesBlock() {
  let props = document.querySelectorAll('.properties .nav-link');

  props.forEach(prop => 
    prop.addEventListener('click', (event) => {
      const elem = event.target;

      if (elem.dataset.id != visibleBlock.dataset.id && elem.dataset.id < 3) {
        visibleBlock.classList.remove('active');
        elem.classList.add('active');

        const previuosVisibleBlock = visibleBlock;
        visibleBlock = elem;        

        document.querySelector(visibleBlock.dataset.element).classList.remove('unvisible');
        document.querySelector(previuosVisibleBlock.dataset.element).classList.add('unvisible');
      }
    })
  );
}

window.onload = () => {
  addActionToPropertiesBlock();
  getRegionInfo(62); 
  getRegionAchievements(62); 
  setActionOnMap();
}