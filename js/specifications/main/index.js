'use strict';

let regionBlock = document.querySelector(".region-info-block");
let regionNameBlock = document.querySelector(".region-name-block");
let descriptionBlock = document.querySelector(".region-description-block");
let selectedRegion = document.querySelector("a.js-svg-map.svg-map--active");
let regionQualityImprovementPlanBlock = document.querySelector(".nav-link.region-quality-improvement-plan");
let regionAchievementsBlock = document.querySelector(".region-achievements-block");
let allInfoBlock = document.querySelector(".all-region-info-block");

let mainControl = document.querySelector("#main-control");
let iframe = document.querySelector("#modal-iframe");
let modalTitle = document.querySelector(".modal-title");

let visibleBlock = document.querySelector('.properties .nav-link.active');
let mainDate = document.querySelector("#main-date");

window.onload = () => {
  mainControl.addEventListener("click", () => {
    setDataForModalIframe("Аппарат управления", "https://gomeloblgaz.by/about/structura/?SECTION_ID=250")  
    $('.modal').modal();
  });

  getAllRegionInfo();
}