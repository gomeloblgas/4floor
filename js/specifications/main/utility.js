'use strict';

function deleteAllChilds(element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}

//получение информации о всей области
function getAllRegionInfo() {
  fetch(`/4floor/server/apis/get-all-region-info.php`)
    .then(response => {
      return response.json();
    })
    .then(resp => {
      if (resp.code == 200) {
        let data = resp.data;
        parseRegionInfo(data, allInfoBlock);
      }
    });
}

const openModalIframe = () => {
  $('.modal').modal();
}

const setDataForModalIframe = (title, link) => {
  modalTitle.innerText = title;
  iframe.src = link;
}


function parseRegionInfo(info, parentElement) {
  deleteAllChilds(parentElement);
  var monthDate = info["month"],
  monthDate = monthDate.length === 1 ? `0${monthDate}` : monthDate; 
  mainDate.innerText = `01.${monthDate}.${info["year"]}`;

  let region = document.createElement("div");
  region.classList.add("all-region");
  region.id = info.id;

  let flatsFirstProps = [{label: "Газифицировано - всего:", value : info["flat-all"]}];
  region.appendChild(createLevelBlockWithProps("first-level", flatsFirstProps, " квартир"));

  let flatsSecondProps = [{label: "природным газом:", value : info["flat-ng"]},
                          {label: "сжиженым газом:", value : info["flat-lg"]}];
  region.appendChild(createLevelBlockWithProps("second-level", flatsSecondProps, " квартир"));

  let flatsThirdProps = [{label: "- ГБУ:", value : info["gby-flat-lg"]},
                         {label: "- ГЕУ:", value : info["gey-flat-lg"]}];
  region.appendChild(createLevelBlockWithProps("third-level", flatsThirdProps, " квартир"));

  //----------------individualFirstProps--------------------
  let individualFirstProps = [{label: "Газифицировано квартир в индивидуальных жилых домах - всего:", value : info["individual-all"]}];
  region.appendChild(createLevelBlockWithProps("first-level", individualFirstProps, " квартир"));

  let individualSecondProps = [{label: "природным газом:", value : info["individual-ng"]},
                          {label: "сжиженым газом:", value : info["individual-lg"]}];
  region.appendChild(createLevelBlockWithProps("second-level", individualSecondProps, " квартир"));

  let individualThirdProps = [{label: "- ГБУ:", value : info["gby-individual-lg"]},
                         {label: "- ГЕУ:", value : info["gey-individual-lg"]}];
  region.appendChild(createLevelBlockWithProps("third-level", individualThirdProps, " квартир"));

  //---------------gasMeterFirstProps---------------------
  let gasMeterFirstProps = [{label: "Количество квартир, оборудованных приборами учета расхода газа:", value : info["gas-meter-all"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gasMeterFirstProps, " квартир"));

  let gasMeterSecondProps = [{label: "природным газом:", value : info["gas-meter-flats-ng"]},
                            {label: "сжиженым газом:", value : info["gas-meter-flats-lg"]},
                          {label: "в том числе в индивидуальных жилых домах на природном газе:", value : info["gas-meter-individuals"]}];
  region.appendChild(createLevelBlockWithProps("second-level", gasMeterSecondProps, " квартир"));

  //-----housesWithApartmentHeating-------------flatsWithApartmentHeating------------------
  let housesWithApartmentHeating = [{label: "Количество многоквартирных жилых домов с поквартирным отоплением:", value : info["houses-with-apartment-heating"]}];
  region.appendChild(createLevelBlockWithProps("first-level", housesWithApartmentHeating, " домов"));

  let flatsWithApartmentHeating = [{label: "в том числе количество квартир в этих домах:", value : info["flats-with-apartment-heating"]}];
  region.appendChild(createLevelBlockWithProps("second-level", flatsWithApartmentHeating, " квартир"));

  //----------------industrialConsumers--------------------
  let industrialConsumers = [{label: "Промышленные потребители и приравненные к ним:", value : info["industrial-consumers"]}];
  region.appendChild(createLevelBlockWithProps("first-level", industrialConsumers, " ед"));

  //---------------nonProductiveConsumerServices---------------------
  let nonProductiveConsumerServices = [{label: "Организации бытового обслуживания населения непроизводственного характера:", value : info["non-productive-consumer-services"]}];
  region.appendChild(createLevelBlockWithProps("first-level", nonProductiveConsumerServices, " ед"));

  //---------------agriculturalConsumers---------------------
  let agriculturalConsumers = [{label: "Сельскохозяйственные потребители:", value : info["agricultural-consumers"]}];
  region.appendChild(createLevelBlockWithProps("first-level", agriculturalConsumers, " ед"));

  //---------------Pipelines---------------------
  let gasPipelinesLength = [{label: "Протяжённость газопроводов:", value : info["gas-pipelines-length"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gasPipelinesLength, " км"));

  //---------------GRP---------------------
  let grp = [{label: "ГРП:", value : info["grp"]}];
  region.appendChild(createLevelBlockWithProps("first-level", grp, " шт"));

  //--------------grp-with-telemechanized----------------------
  let grpWithTelemechanized = [{label: "- телемеханизированные:", value : info["grp-with-telemechanized"]}];
  region.appendChild(createLevelBlockWithProps("second-level", grpWithTelemechanized, " шт"));

  //--------------grp-on-balance----------------------
  let grpOnBalance = [{label: "- на балансе предприятия:", value : info["grp-on-balance"]}];
  region.appendChild(createLevelBlockWithProps("second-level", grpOnBalance, " шт"));

  //--------------SHRP----------------------
  let shrp = [{label: "ШРП:", value : info["shrp"]}];
  region.appendChild(createLevelBlockWithProps("first-level", shrp, " шт"));

  //------------shrp-with-telemechanized---------------------
  let shrpWithTelemechanized = [{label: "- телемеханизированные:", value : info["shrp-with-telemechanized"]}];
  region.appendChild(createLevelBlockWithProps("second-level", shrpWithTelemechanized, " шт"));

  //--------------shrp-on-balance----------------------
  let shrpOnBalance = [{label: "- на балансе предприятия:", value : info["shrp-on-balance"]}];
  region.appendChild(createLevelBlockWithProps("second-level", shrpOnBalance, " шт"));

  //--------------house-regulators----------------------
  let houseRegulators = [{label: "Домовые регуляторы:", value : info["house-regulators"]}];
  region.appendChild(createLevelBlockWithProps("first-level", houseRegulators, " шт"));

  //---------------gas-using-equipment---------------------
  let gasUsingEquipment = [{label: "Бытовое газоиспользующее оборудование:", value : info["gas-using-equipment"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gasUsingEquipment, " шт"));

  //-------------gas-stoves-----------------------
  let gasStoves = [{label: "Газовые плиты:", value : info["gas-stoves"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gasStoves, " шт"));

  //-------------gas-water-heaters-----------------------
  let gasWaterHeaters = [{label: "Водонагреватели (ВПГ) газовые:", value : info["gas-water-heaters"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gasWaterHeaters, " шт"));

  //-------------heating-equipment-----------------------
  let heatingEquipment = [{label: "Отопительное оборудование:", value : info["heating-equipment"]}];
  region.appendChild(createLevelBlockWithProps("first-level", heatingEquipment, " шт"));

  //-----------------skz-------------------
  let skz = [{label: "СКЗ:", value : info["skz"]}];
  region.appendChild(createLevelBlockWithProps("first-level", skz, " шт"));

  //--------------gns----------------------
  let gns = [{label: "ГНС:", value : info["gns"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gns, " шт"));

  //--------------agzs----------------------
  let agzs = [{label: "АГЗС:", value : info["agzs"]}];
  region.appendChild(createLevelBlockWithProps("first-level", agzs, " шт"));

  //--------------gey----------------------
  let gey = [{label: "ГЕУ:", value : info["gey"]}];
  region.appendChild(createLevelBlockWithProps("first-level", gey, " шт"));

  //--------------gey-with-telemechanized----------------------
  let geyWithTelemechanized = [{label: "- телемеханизированные:", value : info["gey-with-telemechanized"]}];
  region.appendChild(createLevelBlockWithProps("second-level", geyWithTelemechanized, " шт"));

  //----Вывод на экран-----
  parentElement.appendChild(region);
}


function createLevelBlockWithProps(classLevel, props, item) {
  let levelBlock = createLevelBlock(classLevel);  
  props.forEach((prop) => {
    let element = createProperties(prop.label, prop.value, item);
    levelBlock.appendChild(element);
  });

  return levelBlock;
}

function createLevelBlock(classLevel) {
  let div = document.createElement('div');
  div.classList.add(classLevel);

  return div;
}

function createProperties(label, value, item) {
  let div = document.createElement('div');
  div.classList.add("row");

  let divLabel = document.createElement('div');
  divLabel.classList.add("col-9");
  divLabel.classList.add("text-left");
  divLabel.innerText = label;

  let divValue = document.createElement('div');
  divValue.classList.add("col-3");
  divValue.classList.add("text-right");
  divValue.classList.add("align-text-bottom");
  divValue.innerText = value + item;

  div.appendChild(divLabel);
  div.appendChild(divValue);

  return div;
}