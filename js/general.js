'use strict';

let timer;
let mainPage = "/4floor/";

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

  
if(urlParams.get('flag') != undefined) {
  document.querySelectorAll('a').forEach( element => {
    element.href += "?flag";
  });
}

document.onkeypress = timerReset;
document.onmousemove = timerReset;
document.onmousedown = timerReset;
document.ontouchstart = timerReset;
document.onclick = timerReset;
document.onscroll = timerReset;
document.onkeypress = timerReset;

function timerElapsed() {
  if(urlParams.get('flag') != undefined) {
    mainPage += "?flag";
  }

  window.location = mainPage;
};

function timerReset() {
  clearTimeout(timer);
  timer = setTimeout(timerElapsed, 60 * 10 * 1000);
}