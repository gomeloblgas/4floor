let regionPropertyNames = ["Код", "Филиал", "Месяц", "Год", "Газифицировано квартир - всего",
                "природным газом", "сжиженным газом, из них", "а) ГБУ", "б) ГЕУ",
                "Газифицировано квартир в индивидуальных жилых домах", "природным газом"];
let audioBlock = document.querySelector(".audio-block");

function addAudio() {
  var audio = document.createElement("AUDIO");

  if (audio.canPlayType("audio/wav")) {
    audio.setAttribute("src","assets/audio/gomelgas-1.wav");
  }
  audio.classList.add("audio-track")
  audio.setAttribute("controls", "controls");
  audioBlock.appendChild(audio);
}