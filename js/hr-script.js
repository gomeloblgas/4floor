

let iframe = document.querySelector("#doc-iframe");
let docName = document.querySelector(".doc-name-viewer");

function openSelectedDocument() {
  let props = document.querySelectorAll('img.doc-logo');

  props.forEach(prop =>
    prop.addEventListener('click', (event) => {
      const elem = event.target;
      iframe.src = elem.title;
    })
  );
}

function toPreviousPage() {
  return document.referrer
}

window.onload = () => {
  openSelectedDocument();
}