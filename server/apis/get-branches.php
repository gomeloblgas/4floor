<?php
include_once('../common/include.php');
$conn = getConnection();

if ($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
} else {
    $branches = [];

    $sql = "SELECT `id`, `name` 
                FROM branches
                WHERE `parrent_id` IS NOT NULL";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $branch = array(
                "id" => $row["id"],
                "name" => $row["name"]
            );
            array_push($branches, $branch);
        }

        sendResponse(200, $branches, 'Branches');
    } else {
        sendResponse(401, [], 'Data is not available');
    }
    $conn->close();
}
?>
