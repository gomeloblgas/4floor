<?php
include_once('../common/include.php');
$conn=getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
} else {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $entityBody = file_get_contents('php://input');
        $params = json_decode($entityBody, true);
        
        $regionID = $params["branch"];
        $date = $params["dateInput"];
        $achievement = $params['achievements'];

        $sql = "INSERT INTO achievements (`region_id`, `description`, `date`)
                VALUES ($regionID, '$achievement', '$date')";

        $fd = fopen("achievements_result.txt", 'w');
        $str = '';

        if (mysqli_query($conn, $sql)) {
            $str = "New achievement created successfully";
            sendResponse(200,[],'Add successfully');
        } else {
            $str = "Error: " . $sql . " \n" . mysqli_error($conn);
            sendResponse(400,[],'SQL error');
        }

        fwrite($fd, $str);
        fclose($fd);
    }

    $conn->close();
}
?>