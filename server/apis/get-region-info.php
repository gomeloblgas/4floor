<?php
include_once('../common/include.php');
$conn = getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
}else{
    $id = $_GET["id"];
    $branches = [];

    $sql = "SELECT `id`, `name` 
                FROM branches
                WHERE `parrent_id` = $id";
    
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $branch = array(
                "id" => $row["id"],
                "name" => $row["name"]
            );
            array_push($branches, $branch);
        }
    }


    $sql = "SELECT general_inform.id, branches.name, general_inform.`month`, general_inform.`year`, general_inform.`flat-ng`, general_inform.`gby-flat-lg`,
                general_inform.`gey-flat-lg`,	general_inform.`individual-ng`,	general_inform.`gby-individual-lg`, general_inform.`gey-individual-lg`,
                general_inform.`gas-meter-flats-ng`, general_inform.`gas-meter-flats-lg`,	general_inform.`gas-meter-individuals`,
                general_inform.`houses-with-apartment-heating`, general_inform.`flats-with-apartment-heating`, general_inform.`industrial-consumers`,
                general_inform.`non-productive-consumer-services`, general_inform.`agricultural-consumers`,	general_inform.`gas-pipelines-length`,
                general_inform.grp,	general_inform.`grp-with-telemechanized`, general_inform.`grp-on-balance`,
                general_inform.shrp, general_inform.`shrp-with-telemechanized`, general_inform.`shrp-on-balance`,
                general_inform.`house-regulators`, general_inform.`gas-stoves`, general_inform.`gas-water-heaters`,
                general_inform.`heating-equipment`, general_inform.skz, general_inform.gns, general_inform.agzs,
                general_inform.gey, general_inform.`gey-with-telemechanized`,  
                branches.`description`,  branches.`link-to-site`,  branches.`creation-date`, 
                achievements.description AS achievements, quality_improvement_plan.name AS `quality-plan`

            FROM general_inform
            LEFT JOIN branches ON `filial-id` = branches.id
            LEFT JOIN achievements ON `filial-id` = achievements.region_id
            LEFT JOIN quality_improvement_plan ON `filial-id` = quality_improvement_plan.`region-id`
            WHERE branches.id = $id 
                AND general_inform.`month` = (SELECT MAX(month) FROM general_inform WHERE `filial-id` = $id AND general_inform.`year` = (SELECT MAX(year) FROM general_inform WHERE `filial-id` = $id)) 
                AND general_inform.`year` = (SELECT MAX(year) FROM general_inform WHERE `filial-id` = $id) ";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        $region;
        while($row = $result->fetch_assoc()) {
            $region = array(
              "id" => $row["id"],
              "filial" => $row["name"],
              "flat-all" => $row["flat-ng"] + $row["gby-flat-lg"] + $row["gey-flat-lg"],
              "flat-ng" => $row["flat-ng"],
              "flat-lg" => $row["gby-flat-lg"] + $row["gey-flat-lg"],
              "gby-flat-lg" => $row["gby-flat-lg"],
              "gey-flat-lg" => $row["gey-flat-lg"],
              "individual-all" => $row["individual-ng"] + $row["gby-individual-lg"] + $row["gey-individual-lg"],
              "individual-ng" => $row["individual-ng"],
              "individual-lg" => $row["gby-individual-lg"] + $row["gey-individual-lg"],
              "gby-individual-lg" => $row["gby-individual-lg"],
              "gey-individual-lg" => $row["gey-individual-lg"],
              "gas-meter-all" => $row["gas-meter-flats-ng"] + $row["gas-meter-flats-lg"] + $row["gas-meter-individuals"],
              "gas-meter-flats-ng" => $row["gas-meter-flats-ng"],
              "gas-meter-flats-lg" => $row["gas-meter-flats-lg"],
              "gas-meter-individuals" => $row["gas-meter-individuals"],
              "houses-with-apartment-heating" => $row["houses-with-apartment-heating"], 
              "flats-with-apartment-heating" => $row["flats-with-apartment-heating"],
              "industrial-consumers" => $row["industrial-consumers"],
              "non-productive-consumer-services" => $row["non-productive-consumer-services"],
              "agricultural-consumers" => $row["agricultural-consumers"],
              "gas-pipelines-length" => $row["gas-pipelines-length"],
              "grp" => $row["grp"],
              "grp-with-telemechanized" => $row["grp-with-telemechanized"],
              "grp-on-balance" => $row["grp-on-balance"],
              "shrp" => $row["shrp"],
              "shrp-with-telemechanized" => $row["shrp-with-telemechanized"],
              "shrp-on-balance" => $row["shrp-on-balance"],
              "house-regulators" => $row["house-regulators"],
              "gas-using-equipment" => $row["gas-stoves"] + $row["gas-water-heaters"] + $row["heating-equipment"],
              "gas-stoves" => $row["gas-stoves"],
              "gas-water-heaters" => $row["gas-water-heaters"],
              "heating-equipment" => $row["heating-equipment"],
              "skz" => $row["skz"],
              "gns" => $row["gns"],
              "agzs" => $row["agzs"],
              "gey" => $row["gey"],
              "gey-with-telemechanized" => $row["gey-with-telemechanized"],
              "description" => $row["description"],
              "link" => $row["link-to-site"],
              "creation-date" => $row["creation-date"],
              "achievements" => $row["achievements"],
              "quality-plan" => $row["quality-plan"],
              "branches" => $branches
            );
        }

        sendResponse(200, $region, 'Region Info');
    } else {
        sendResponse(404,[],'Data is not available');
    }
    $conn->close();
}
?>
