<?php
include_once('../common/include.php');
$conn=getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
}else{    
    $sql = "SELECT general_inform.`month`, general_inform.`year`, SUM(general_inform.`flat-ng`) as `flat-ng`, SUM(general_inform.`gby-flat-lg`) as `gby-flat-lg`,
                SUM(general_inform.`gey-flat-lg`) as `gey-flat-lg`,	SUM(general_inform.`individual-ng`) as `individual-ng`,
                SUM(general_inform.`gby-individual-lg`) as `gby-individual-lg`,	SUM(general_inform.`gey-individual-lg`) as `gey-individual-lg`,
                SUM(general_inform.`gas-meter-flats-ng`) as `gas-meter-flats-ng`, SUM(general_inform.`gas-meter-flats-lg`) as `gas-meter-flats-lg`,	
                SUM(general_inform.`gas-meter-individuals`) as `gas-meter-individuals`, SUM(general_inform.`houses-with-apartment-heating`) as `houses-with-apartment-heating`, 
                SUM(general_inform.`flats-with-apartment-heating`) as `flats-with-apartment-heating`, SUM(general_inform.`industrial-consumers`) as `industrial-consumers`,
                SUM(general_inform.`non-productive-consumer-services`) as `non-productive-consumer-services`, SUM(general_inform.`agricultural-consumers`) as `agricultural-consumers`,	
                SUM(general_inform.`gas-pipelines-length`) as `gas-pipelines-length`, SUM(general_inform.grp) as grp, 
                SUM(general_inform.`grp-with-telemechanized`) as `grp-with-telemechanized`, SUM(general_inform.`grp-on-balance`) as `grp-on-balance`,
                SUM(general_inform.shrp) as shrp, SUM(general_inform.`shrp-with-telemechanized`) as `shrp-with-telemechanized`, SUM(general_inform.`shrp-on-balance`) as `shrp-on-balance`,
                SUM(general_inform.`house-regulators`) as `house-regulators`, 
                SUM(general_inform.`gas-stoves`) as `gas-stoves`, SUM(general_inform.`gas-water-heaters`) as `gas-water-heaters`,
                SUM(general_inform.`heating-equipment`) as `heating-equipment`, SUM(general_inform.skz) as skz, SUM(general_inform.gns) as gns, 
                SUM(general_inform.agzs) as agzs, SUM(general_inform.gey) as gey, SUM(general_inform.`gey-with-telemechanized`) as `gey-with-telemechanized`

            FROM general_inform
            WHERE general_inform.`month` = (SELECT MAX(month) FROM general_inform WHERE `year` = (SELECT MAX(year) FROM general_inform))
                AND general_inform.`year` = (SELECT MAX(year) FROM general_inform)";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        $region;
        while($row = $result->fetch_assoc()) {
            $region = array(
              "month" => $row["month"],
              "year" => $row["year"],  
              "flat-all" => $row["flat-ng"] + $row["gby-flat-lg"] + $row["gey-flat-lg"],
              "flat-ng" => $row["flat-ng"],
              "flat-lg" => $row["gby-flat-lg"] + $row["gey-flat-lg"],
              "gby-flat-lg" => $row["gby-flat-lg"],
              "gey-flat-lg" => $row["gey-flat-lg"],
              "individual-all" => $row["individual-ng"] + $row["gby-individual-lg"] + $row["gey-individual-lg"],
              "individual-ng" => $row["individual-ng"],
              "individual-lg" => $row["gby-individual-lg"] + $row["gey-individual-lg"],
              "gby-individual-lg" => $row["gby-individual-lg"],
              "gey-individual-lg" => $row["gey-individual-lg"],
              "gas-meter-all" => $row["gas-meter-flats-ng"] + $row["gas-meter-flats-lg"] + $row["gas-meter-individuals"],
              "gas-meter-flats-ng" => $row["gas-meter-flats-ng"],
              "gas-meter-flats-lg" => $row["gas-meter-flats-lg"],
              "gas-meter-individuals" => $row["gas-meter-individuals"],
              "houses-with-apartment-heating" => $row["houses-with-apartment-heating"], 
              "flats-with-apartment-heating" => $row["flats-with-apartment-heating"],
              "industrial-consumers" => $row["industrial-consumers"],
              "non-productive-consumer-services" => $row["non-productive-consumer-services"],
              "agricultural-consumers" => $row["agricultural-consumers"],
              "gas-pipelines-length" => $row["gas-pipelines-length"],
              "grp" => $row["grp"],
              "grp-with-telemechanized" => $row["grp-with-telemechanized"],
              "grp-on-balance" => $row["grp-on-balance"],
              "shrp" => $row["shrp"],
              "shrp-with-telemechanized" => $row["shrp-with-telemechanized"],
              "shrp-on-balance" => $row["shrp-on-balance"],
              "house-regulators" => $row["house-regulators"],
              "gas-using-equipment" => $row["gas-stoves"] + $row["gas-water-heaters"] + $row["heating-equipment"],
              "gas-stoves" => $row["gas-stoves"],
              "gas-water-heaters" => $row["gas-water-heaters"],
              "heating-equipment" => $row["heating-equipment"],
              "skz" => $row["skz"],
              "gns" => $row["gns"],
              "agzs" => $row["agzs"],
              "gey" => $row["gey"],
              "gey-with-telemechanized" => $row["gey-with-telemechanized"]
            );
        }

        sendResponse(200, $region, 'Region Info');
    } else {
        sendResponse(404,[],'Data is not available');
    }
    $conn->close();
}
?>
