<?php
include_once('../common/include.php');
$conn=getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
} else {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $entityBody = file_get_contents('php://input');
        $params = json_decode($entityBody, true);
        
        $gasSold = $params["gas-sold"];
        $gasSalesPlan = $params["gas-sales-plan"];
        $currentMonth = $params['current-month'];

        $sql = "INSERT INTO `gas-counter` (`gas-sold`, `gas-sales-plan`, `current-month`)
                VALUES ($gasSold, '$gasSalesPlan', '$currentMonth')";

        $fd = fopen("gas_counter_result.txt", 'w');
        $str = '';

        if (mysqli_query($conn, $sql)) {
            $str = "Created successfully! \n gas-sold: $gasSold; \n gas-sales-plan: $gasSalesPlan;  \n gas-sales-plan: $gasSalesPlan;";
            sendResponse(200,[],'Add successfully');
        } else {
            $str = "Error: " . $sql . " \n" . mysqli_error($conn);
            sendResponse(400,[],'SQL error');
        }

        fwrite($fd, $str);
        fclose($fd);
    }

    $conn->close();
}
?>