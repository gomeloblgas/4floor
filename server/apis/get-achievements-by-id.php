<?php
include_once('../common/include.php');
$conn=getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
}else{
    $id = $_GET["id"];
    
    $sql = "SELECT *
              FROM achievements
              WHERE region_id = $id OR region_id = 0
              ORDER BY `date` DESC";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        $achievements = [];
        while($row = $result->fetch_assoc()) {
            $achievement = array(
              "id" => $row["id"],
              "region_id" => $row["region_id"],
              "description" => $row["description"],
              "date" => $row["date"]
            );
            array_push($achievements, $achievement);
        }

        sendResponse(200, $achievements, 'Region Info');
    } else {
        sendResponse(404,[],'Data is not available');
    }
    $conn->close();
}
?>