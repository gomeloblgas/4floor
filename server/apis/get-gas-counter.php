<?php
include_once('../common/include.php');
$conn=getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
}else{    
    $sql = "SELECT *
              FROM `gas_counter`";
    $result = $conn->query($sql);

    if ($result->num_rows == 1) {
        sendResponse(200, $result->fetch_assoc(), 'Gas counter');
    } else {
        sendResponse(404,[],'Data is not available');
    }
    $conn->close();
}
?>