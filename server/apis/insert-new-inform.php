<?php
include_once('../common/include.php');
$conn=getConnection();

if($conn==null){
    sendResponse(500,$conn,'Server Connection Error');
}else{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $entityBody = file_get_contents('php://input');
        $params = json_decode($entityBody, true);
        
        $branchID = $params["branch"];
        $month = $params["month"];
        $year = $params['year'];

        $flatNg = $params["flat-ng"];
        $gbyFlatLg = $params["gby-flat-lg"];
        $geyFlatLg = $params["gey-flat-lg"];
        $individualNg = $params["individual-ng"];
        $gbyIndividualLg = $params["gby-individual-lg"];
        $geyIndividualLg = $params["gey-individual-lg"];
        $gasMeterFlatsNg = $params["gas-meter-flats-ng"];
        $gasMeterFlatsLg = $params["gas-meter-flats-lg"];
        $gasMeterIndividuals = $params["gas-meter-individuals"];
        $housesWithApartmentHeating = $params["houses-with-apartment-heating"]; 
        $flatsWithApartmentHeating = $params["flats-with-apartment-heating"];
        $industrialConsumers = $params["industrial-consumers"];
        $nonProductiveConsumerServices = $params["non-productive-consumer-services"];
        $agriculturalConsumers = $params["agricultural-consumers"];
        $gasPipelinesLength = $params["gas-pipelines-length"];
        $grp = $params["grp"];
        $grpWithTelemechanized = $params["grp-with-telemechanized"];
        $shrp = $params["shrp"];
        $shrpWithTelemechanized = $params["shrp-with-telemechanized"];
        $houseRegulators = $params["house-regulators"];
        $grpOnBalance = $params["grp-on-balance"];
        $shrpOnBalance = $params["shrp-on-balance"];
        $gasStoves = $params["gas-stoves"];
        $gasWaterHeaters = $params["gas-water-heaters"];
        $heatingEquipment = $params["heating-equipment"];
        $skz = $params["skz"];
        $gns = $params["gns"];
        $agzs = $params["agzs"];
        $gey = $params["gey"];
        $geyWithTelemechanized = $params["gey-with-telemechanized"];
    

        $sql = "SET @filialId = $branchID,
                    @year = $year,
                    @month = $month,
                    @flatNg = $flatNg,
                    @gbyFlatLg = $gbyFlatLg,
                    @geyFlatLg = $geyFlatLg,
                    @individualNg = $individualNg,
                    @gbyIndividualLg = $gbyIndividualLg,
                    @geyIndividualLg = $geyIndividualLg,
                    @gasMeterFlatsNg = $gasMeterFlatsNg,
                    @gasMeterFlatsLg = $gasMeterFlatsLg,
                    @gasMeterIndividuals = $gasMeterIndividuals,
                    @housesWithApartmentHeating = $housesWithApartmentHeating,
                    @flatsWithApartmentHeating = $flatsWithApartmentHeating,
                    @industrialConsumers = $industrialConsumers,
                    @nonProductiveConsumerServices = $nonProductiveConsumerServices,
                    @agriculturalConsumers = $agriculturalConsumers,
                    @gasPipelinesLength = $gasPipelinesLength,

                    @grp = $grp,
                    @grpWithTelemechanized = $grpWithTelemechanized,
                    @grpOnBalance = $grpOnBalance,
                    @shrp = $shrp,
                    @shrpWithTelemechanized = $shrpWithTelemechanized,
                    @shrpOnBalance = $shrpOnBalance, 
                    @houseRegulators = $houseRegulators,

                    @gasStoves = $gasStoves,
                    @gasWaterHeaters = $gasWaterHeaters,
                    @heatingEquipment = $heatingEquipment,
                    @skz = $skz,
                    @gns = $gns,
                    @agzs = $agzs,
                    @gey = $gey,
                    @geyWithTelemechanized = $geyWithTelemechanized;";
        mysqli_query($conn, $sql);
        $sql = "INSERT INTO general_inform (`filial-id`, `year`, `month`,
                                                `flat-ng`, `gby-flat-lg`, `gey-flat-lg`,
                                                `individual-ng`, `gby-individual-lg`, `gey-individual-lg`,
                                                `gas-meter-flats-ng`, `gas-meter-flats-lg`, `gas-meter-individuals`,
                                                `houses-with-apartment-heating`, `flats-with-apartment-heating`, 
                                                `industrial-consumers`, `non-productive-consumer-services`, `agricultural-consumers`,
                                                `gas-pipelines-length`, grp, `grp-with-telemechanized`, `grp-on-balance`,
                                                shrp, `shrp-with-telemechanized`, `shrp-on-balance`,

                                                `house-regulators`, `gas-stoves`,
                                                `gas-water-heaters`, `heating-equipment`, skz, gns,
                                                agzs, gey, `gey-with-telemechanized`)

                        VALUES (@filialId, @year, @month, 
                                @flatNg, @gbyFlatLg, @geyFlatLg,
                                @individualNg, @gbyIndividualLg, @geyIndividualLg,
                                @gasMeterFlatsNg, @gasMeterFlatsLg, @gasMeterIndividuals,
                                @housesWithApartmentHeating, @flatsWithApartmentHeating,
                                @industrialConsumers, @nonProductiveConsumerServices, @agriculturalConsumers,
                                @gasPipelinesLength, @grp, @grpWithTelemechanized, @grpOnBalance,
                                @shrp, @shrpWithTelemechanized, @shrpOnBalance,

                                @houseRegulators, @gasStoves,
                                @gasWaterHeaters, @heatingEquipment, @skz, @gns,
                                @agzs, @gey, @geyWithTelemechanized)

                        ON DUPLICATE KEY UPDATE
                                general_inform.`flat-ng` = @flatNg,
                                general_inform.`gby-flat-lg` = @gbyFlatLg,
                                general_inform.`gey-flat-lg` = @geyFlatLg,
                                general_inform.`individual-ng` = @individualNg,
                                general_inform.`gby-individual-lg` = @gbyIndividualLg,
                                general_inform.`gey-individual-lg` = @geyIndividualLg,
                                general_inform.`gas-meter-flats-ng` = @gasMeterFlatsNg,
                                general_inform.`gas-meter-flats-lg` = @gasMeterFlatsLg,
                                general_inform.`gas-meter-individuals` = @gasMeterIndividuals,
                                general_inform.`houses-with-apartment-heating` = @housesWithApartmentHeating,
                                general_inform.`flats-with-apartment-heating` = @flatsWithApartmentHeating,
                                general_inform.`industrial-consumers` = @industrialConsumers,
                                general_inform.`non-productive-consumer-services` = @nonProductiveConsumerServices,
                                general_inform.`agricultural-consumers` = @agriculturalConsumers,
                                general_inform.`gas-pipelines-length` = @gasPipelinesLength,
                                general_inform.grp = @grp,
                                general_inform.`grp-with-telemechanized` = @grpWithTelemechanized,
                                general_inform.`grp-on-balance` = @grpOnBalance,
                                general_inform.shrp = @shrp,
                                general_inform.`shrp-with-telemechanized` = @shrpWithTelemechanized,
                                general_inform.`shrp-on-balance` = @shrpOnBalance,
                                general_inform.`house-regulators` = @houseRegulators,
                                
                                general_inform.`gas-stoves` = @gasStoves,
                                general_inform.`gas-water-heaters` = @gasWaterHeaters,
                                general_inform.`heating-equipment` = @heatingEquipment,
                                general_inform.skz = @skz,
                                general_inform.gns = @gns,
                                general_inform.agzs = @agzs,
                                general_inform.gey = @gey,
                                general_inform.`gey-with-telemechanized` = @geyWithTelemechanized";

        $fd = fopen("feedback_result.txt", 'w');
        $str = '';

        if (mysqli_query($conn, $sql)) {
            $str = "New feedback created successfully";
            sendResponse(200,[],'Add successfully');
        } else {
            $str = "Error: " . $sql . " \n" . mysqli_error($conn);
            sendResponse(400,[],'SQL error');
        }

        fwrite($fd, $str);
        fclose($fd);
    }

    $conn->close();
}
?>
