'use strict';

function cleanSelect(select, title) {
  while (select.firstChild) {
    select.removeChild(select.firstChild);
  }

  let option = document.createElement('option');
  option.innerText = title;
  option.disabled = true
  option.selected = true

  select.appendChild(option);
}

function getbranches() {
  fetch(`/4floor/server/apis/get-branches.php`)
    .then(response => {
      return response.json();
    })
    .then(branches => {
      localStorage.setItem('branches', JSON.stringify(branches.data));
      parseBranches(branches.data);
    });
}

function parseBranches(branches) {
  cleanSelect(branchesSelect, 'Выберите филиал');
  
  let option = document.createElement('option');
  option.innerText = "Все филиалы";
  option.value = 0;
  option.dataValue = 0;
  branchesSelect.appendChild(option);
  
  branches.forEach(element => {
    let option = document.createElement('option');
    option.innerText = element['name'];
    option.value = element.id;
    option.dataValue = element['id'];

    branchesSelect.appendChild(option);
  });
}

//Получение всех полей и отправка на сервер
function insertNewAchievement(post) {
  console.log(post);
  let url = "/4floor/server/apis/insert-new-achievement.php";

  try {
    postData(url, post)
      .then((data) => {
        if (data.code == 200) {
          console.log("Success");
          console.log(data);
          let message = `<p>${branchesSelect.selectedOptions[0].label} на дату ${dateInput.value}</p> <br>${achievementsFormatedText()}`;
          fields.forEach( element => {
            element.value = "";
          });
          getbranches();
          addNotification(true, message);
        } else {
          console.log("Error");
          console.log(data);
        addNotification(false);
        }
      });
  } catch (err) {
    console.log(err);
    console.log(data);
  }
  
}


async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}


//Уведомление о отправке запроса
function addNotification(flag, message = '')  { 
  let className = ''; 
  if (flag) {
    className = 'alert-success';
    completeModal.querySelector('.modal-title').innerText = 'Данные внесены!';
    completeModal.querySelector('.modal-body').innerHTML = message;	
  } else {
    className = 'alert-danger';
    completeModal.querySelector('.modal-title').innerText = 'Упс!';
    completeModal.querySelector('.modal-body').innerHTML = '<p>Возникли проблемы, попробуйте отправить позже.</p>';	
  }
  
  $('div#completeModal').modal('show')
}

// для примера
function getbrancheById(id) {
  fetch(`/4floor/server/apis/get-branch-by-id.php?id=${id}`)
    .then(response => {
      return response.json();
    })
    .then(branches => {
      let branch = branches.data;
      console.log(branch);

      fields.forEach( element => {
        if (element.id != 'branch')
          element.value = '';
      });      

      monthsSelect.value = branch.month;
      yearsSelect.value = branch.year;

      fields.forEach( element => {
        element.placeholder = branch[element.id];
        if (changeFlag.checked && element.id != 'branch') {
          element.value = branch[element.id];
        }
      });
    });
}
