'use strict';

let form = document.querySelector('#maintenanceForm');

let branchesSelect = document.querySelector('select#branch');
let dateInput = document.querySelector('input#dateInput');
let achievementsTextArea = document.querySelector('textarea#achievements');

let saveButton = document.querySelector('.save-button');

let completeModal = document.querySelector('div#completeModal');

const fields = [branchesSelect, dateInput, achievementsTextArea];

const achievementsFormatedText = () => {
  return "<p>" + achievementsTextArea.value.replaceAll("\n", "</p><p>") + "</p>"
}


window.onload = () => {
  getbranches();

  form.addEventListener('submit', (event) => {
    event.preventDefault();
  
    let post = {};
    fields.forEach( element => {
      post[element.id] = element.value;
    });
    post[achievementsTextArea.id] = achievementsFormatedText();
    
    insertNewAchievement(post);

    return false
  });
}