'use strict';

let form = document.querySelector('#maintenanceForm');

let branchesSelect = document.querySelector('select#branch');
let yearsSelect = document.querySelector('select#year');
let monthsSelect = document.querySelector('select#month');

let flatNg = document.querySelector('input#flat-ng');
let gbyFlatLg = document.querySelector('input#gby-flat-lg');
let geyFlatLg = document.querySelector('input#gey-flat-lg');

let individualNg = document.querySelector('input#individual-ng');
let gbyIndividualLg = document.querySelector('input#gby-individual-lg');
let geyIndividualLg = document.querySelector('input#gey-individual-lg');

let gasMeterFlatsNg = document.querySelector('input#gas-meter-flats-ng');
let gasMeterFlatsLg = document.querySelector('input#gas-meter-flats-lg');
let gasMeterIndividuals = document.querySelector('input#gas-meter-individuals');

let housesWithApartmentHeating = document.querySelector('input#houses-with-apartment-heating');
let flatsWithApartmentHeating = document.querySelector('input#flats-with-apartment-heating');

let industrialConsumers = document.querySelector('input#industrial-consumers');
let nonProductiveConsumerServices = document.querySelector('input#non-productive-consumer-services');
let agriculturalConsumers = document.querySelector('input#agricultural-consumers');

let gasPipelinesLength = document.querySelector('input#gas-pipelines-length');

let grp = document.querySelector('input#grp');
let grpWithTelemechanized = document.querySelector('input#grp-with-telemechanized');
let grpOnBalance = document.querySelector('input#grp-on-balance');

let shrp = document.querySelector('input#shrp');
let shrpWithTelemechanized = document.querySelector('input#shrp-with-telemechanized');
let shrpOnBalance = document.querySelector('input#shrp-on-balance');

let houseRegulators = document.querySelector('input#house-regulators');
let gasStoves = document.querySelector('input#gas-stoves');
let gasWaterHeaters = document.querySelector('input#gas-water-heaters');
let heatingEquipment = document.querySelector('input#heating-equipment');

let skz = document.querySelector('input#skz');
let gns = document.querySelector('input#gns');
let agzs = document.querySelector('input#agzs');
let gey = document.querySelector('input#gey');
let geyWithTelemechanized = document.querySelector('input#gey-with-telemechanized');

let saveButton = document.querySelector('.save-button');

let completeModal = document.querySelector('div#completeModal');

const fields = [branchesSelect, yearsSelect, monthsSelect, flatNg, gbyFlatLg, geyFlatLg, individualNg, gbyIndividualLg, geyIndividualLg,
                gasMeterFlatsNg, gasMeterFlatsLg, gasMeterIndividuals, housesWithApartmentHeating, flatsWithApartmentHeating,
                industrialConsumers, nonProductiveConsumerServices, agriculturalConsumers, gasPipelinesLength,
                grp, grpWithTelemechanized, grpOnBalance, shrp, shrpWithTelemechanized, shrpOnBalance, houseRegulators, 
                gasStoves, gasWaterHeaters, heatingEquipment, skz, gns, agzs, gey, geyWithTelemechanized];

function generateMonths() {  
  months.forEach(m =>{ 
    let option = document.createElement('option');
    option.innerText = m.name;
    option.value = m.id;
    option.dataValue = m.id;

    monthsSelect.appendChild(option);
  });
}

function generateYears() {
  let today = new Date();
  let year = today.getFullYear() - 1;
  
  for (let i = year; i < (year + 3); i++) {
    console.log(i);

    let option = document.createElement('option');
    option.innerText = i;
    option.value = i;
    option.dataValue = i;

    yearsSelect.appendChild(option);
  }
}  


window.onload = () => {
  generateYears();
  generateMonths();
  getbranches();

  branchesSelect.addEventListener("change", (event) => {
    let id = event.target.selectedOptions["0"].value;
    getbrancheById(id);  
  });

  document.querySelector("#dateAndRegion").addEventListener("mouseenter", (event) => {
    $('[data-toggle="tooltip"]').tooltip();
  });

  form.addEventListener('submit', (event) => {
    event.preventDefault();  
    insertNewBranchInform();

    return false
  });
}
