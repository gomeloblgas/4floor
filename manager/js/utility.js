'use strict';

function cleanSelect(select, title) {
  while (select.firstChild) {
    select.removeChild(select.firstChild);
  }

  let option = document.createElement('option');
  option.innerText = title;
  option.disabled = true
  option.selected = true

  select.appendChild(option);
}

function getbrancheById(id) {
  fetch(`/4floor/server/apis/get-branch-by-id.php?id=${id}`)
    .then(response => {
      return response.json();
    })
    .then(branches => {
      let branch = branches.data;
      console.log(branch);

      fields.forEach( element => {
        if (element.id != 'branch')
          element.value = '';
      });      

      monthsSelect.value = branch.month;
      yearsSelect.value = branch.year;

      fields.forEach( element => {
        element.placeholder = branch[element.id];
        if (changeFlag.checked && element.id != 'branch') {
          element.value = branch[element.id];
        }
      });
    });
}


function getbranches() {
  fetch(`/4floor/server/apis/get-branches.php`)
    .then(response => {
      return response.json();
    })
    .then(branches => {
      localStorage.setItem('branches', JSON.stringify(branches.data));
      parseBranches(branches.data);
    });
}

function parseBranches(branches) {
  cleanSelect(branchesSelect, 'Выберите филиал');

  branches.forEach(element => {
    let option = document.createElement('option');
    option.innerText = element['name'];
    option.value = element.id;
    option.dataValue = element['id'];

    branchesSelect.appendChild(option);
  });
}

//Получение всех полей и отправка на сервер
function insertNewBranchInform() {
  let post = {};
  fields.forEach( element => {
    post[element.id] = element.value;
  });
  console.log(post);

  let url = "/4floor/server/apis/insert-new-inform.php";

  try {
    postData(url, post)
      .then((data) => {
        if (data.code == 200) {
          console.log("Success");
          console.log(data);
          let message = `<p>${branchesSelect.selectedOptions[0].label} на ${monthsSelect.selectedOptions[0].label} ${yearsSelect.selectedOptions[0].label}</p>`;
          fields.forEach( element => {
            element.value = "";
          });
          addNotification(true, message);
        } else {
          console.log("Error");
          console.log(data);
        addNotification(false);
        }
      });
  } catch (err) {
    console.log(err);
    console.log(data);
  }
  
}


async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}


//Уведомление о отправке запроса
function addNotification(flag, message = '')  { 
  let className = ''; 
  if (flag) {
    className = 'alert-success';
    completeModal.querySelector('.modal-title').innerText = 'Данные внесены!';
    completeModal.querySelector('.modal-body').innerHTML = message;	
  } else {
    className = 'alert-danger';
    completeModal.querySelector('.modal-title').innerText = 'Упс!';
    completeModal.querySelector('.modal-body').innerHTML = '<p>Возникли проблемы, попробуйте отправить позже.</p>';	
  }
  
  $('div#completeModal').modal('show')
}


const months = [
  {
    id: 1,
    name: "Январь"
  },
  {
    id: 2,
    name: "Февраль"
  },
  {
    id: 3,
    name: "Март"
  },
  {
    id: 4,
    name: "Апрель"
  },
  {
    id: 5,
    name: "Май"
  },
  {
    id: 6,
    name: "Июнь"
  },
  {
    id: 7,
    name: "Июль"
  },
  {
    id: 8,
    name: "Август"
  },
  {
    id: 9,
    name: "Сентябрь"
  },
  {
    id: 10,
    name: "Октябрь"
  },
  {
    id: 11,
    name: "Ноябрь"
  },
  {
    id: 12,
    name: "Декабрь"
  }];